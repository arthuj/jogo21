package br.com.itau;

import java.util.ArrayList;
import java.util.Scanner;

public class Impressora {
    Scanner scanner;

    public Impressora() {
    }

    public int imprimir()
    {
        System.out.println("Aperte 1 - Retirar Carta");
        System.out.println("Aperte 2 - Desistir");
        System.out.println("Aperte 3 - Visualizar pontuação atual");
        System.out.println("Aperte 4 - Visualizar melhor pontuação");
        scanner = new Scanner(System.in);
        return  scanner.nextInt();
    }

    public  void imprimirPontuacaoAtual(int  pontuacao)
    {
        System.out.println("Sua pontuação atual é " + pontuacao);
    }

    public  void imprimirHistorico(int historico)
    {
        System.out.println("Seu histórico é " + historico);
    }

    public  void  imprimirCarta(Carta carta)
    {
        System.out.println("Você tirou a carta " + carta.getNumero() + " de " + carta.getNaipe());
    }

    public  void imprimirBaralho(ArrayList<Carta> baralho)
    {
        for (Carta carta: baralho
             ) {
            System.out.println(carta.numero);
            System.out.println(carta.naipe);
            System.out.println(carta.valor);
        }

    }
}
