package br.com.itau;

public enum Naipe {
    Copas,
    Paus,
    Espadas,
    Ouro;
}
