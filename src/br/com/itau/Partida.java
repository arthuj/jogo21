package br.com.itau;

import java.util.Scanner;

public class Partida {

    protected int historico;
    protected int pontosAtual;
    private Impressora impressora = new Impressora();
    Baralho baralho;

    public Partida() {
        historico = 0;
        iniciarPartida();
    }
    public void iniciarPartida(){
        pontosAtual = 0;
        this.baralho = new Baralho();
        inicioJogo();
        Rodada();
    }

    private void Rodada()
    {

        switch (impressora.imprimir())
        {
            case 1:
                retirarCarta();
                break;
            case 2:
                desistir();
                break;
            case 3:
                impressora.imprimirPontuacaoAtual(pontosAtual);
                Rodada();
                break;
            case 4:
                impressora.imprimirHistorico(historico);
                Rodada();
                break;
            default:
                System.out.println("Aperte um número da ações abaixo");
                Rodada();
        }
    }

    private  void inicioJogo()
    {
        pontosAtual += baralho.retirarCarta();
        pontosAtual += baralho.retirarCarta();
        impressora.imprimirPontuacaoAtual(pontosAtual);
    }

    private void  retirarCarta()
    {
        pontosAtual += baralho.retirarCarta();
        impressora.imprimirPontuacaoAtual(pontosAtual);
        if(pontosAtual > 21) {
            System.out.println("Você perdeu, reiniciando jogo...");
            iniciarPartida();
        }
        else
            Rodada();
    }

    private void desistir()
    {
        impressora.imprimirPontuacaoAtual(pontosAtual);
        if(pontosAtual > historico) {
            System.out.println("Seu histórico foi atualizado!");
            historico = pontosAtual;
            impressora.imprimirHistorico(historico);
        }
        System.out.println("Reiniciando partida...");
        iniciarPartida();

    }

    private  void  FimPartida()
    {
        System.out.println();
    }



    public int getHistorico() {
        return historico;
    }

    public void setHistorico(int historico) {
        this.historico = historico;
    }

    public int getPontosAtual() {
        return pontosAtual;
    }

    public void setPontosAtual(int pontosAtual) {
        this.pontosAtual = pontosAtual;
    }
}
