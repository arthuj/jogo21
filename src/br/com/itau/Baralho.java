package br.com.itau;

import java.util.ArrayList;
import java.util.Collections;

public class Baralho {

    protected ArrayList<Carta> baralho;
    public  Impressora impressora = new Impressora();
    private int index;

    public Baralho() {
        index = 0;
        baralho = new ArrayList<Carta>();
        criarBaralho();
    }

    public void criarBaralho()
    {

        for(Naipe naipe : Naipe.values())
        {
            for(Numeros numeros : Numeros.values())
            {
                int valor;
                Carta carta = new Carta(numeros.name(),naipe.name(),numeros.valor);
                this.baralho.add(carta);
            }
        }
        //impressora.imprimirBaralho(baralho);
        embaralhar();
    }

    public void embaralhar()
    {
        Collections.shuffle(this.baralho);
    }

    public int retirarCarta()
    {
        impressora.imprimirCarta(baralho.get(index));
        int pontos = this.baralho.get(index).valor;
        index++;
        return pontos;
    }

}
